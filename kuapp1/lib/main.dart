import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:kuapp/web_view.dart';
import 'package:url_launcher/url_launcher.dart';

import 'Animation/FadeAnimation.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'KU APP',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _url = 'https://kbbet11.info';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FadeAnimation(
                0.5,
                Container(
                  height: MediaQuery.of(context).size.height/2,
                    margin: const EdgeInsets.only(top: 80, left: 80, right: 80),
                    child: Image.asset("assets/images/logoku.png"))),
            FadeAnimation(
              3,
              Container(
                margin: const EdgeInsets.only(left: 80.0, right: 80.0, top: 30),
                height: 50,
                child: OutlinedButton(
                  style: OutlinedButton.styleFrom(
                      padding: EdgeInsets.only(left: 15, right: 10),
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      backgroundColor: Colors.red),
                  child: Center(
                    child: Text(
                      "TRANG CHỦ",
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                          fontSize: 25),
                    ),
                  ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => WebViewExample()),
                      );
                    }
                ),
              ),
            ),
            FadeAnimation(
              3,
              Container(
                margin: const EdgeInsets.only(left: 80.0, right: 80.0, top: 30),
                height: 50,
                child: OutlinedButton(
                  style: OutlinedButton.styleFrom(
                      padding: EdgeInsets.only(left: 15, right: 10),
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      backgroundColor: Colors.blue),
                  child: Center(
                    child: Text(
                      "HỖ TRỢ",
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                          fontSize: 25),
                    ),
                  ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => WebViewExample()),
                      );
                    }
                ),
              ),
            ),
            Spacer(),
          ],
        ),
      ),
    ); // This trailing comma makes auto-formatting nicer for build methods
  }

  void _launchURL() async => await canLaunch(_url)
      ? await launch(_url)
      : throw 'Could not launch $_url';
}
